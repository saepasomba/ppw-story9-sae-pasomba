from django.urls import path
from . import views

app_name = 'logInAndGreet'

urlpatterns = [
    path('', views.logInAndGreetIndex, name='logInAndGreetIndex'),
    path('logout/', views.logOutAcc, name='logOutAcc'),
    path('signup/', views.signUpAcc, name='signUpAcc')
]
