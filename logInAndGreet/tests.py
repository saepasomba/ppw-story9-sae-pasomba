from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

from .views import *
from .models import *
from .apps import *

import time
import os


class LogInUnitTest(TestCase):

    def test_apps(self):
        self.assertEqual(LoginandgreetConfig.name, 'logInAndGreet')
        self.assertEqual(apps.get_app_config('logInAndGreet').name, 'logInAndGreet')

    def test_logInAndGreet_routing_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_logInAndGreet_function_correct(self):
        found = resolve('/login/')
        self.assertEqual(found.func, logInAndGreetIndex)

    def test_logInAndGreet_render_correct_html(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_logInAndGreet_signup_routing_exist(self):
        response = Client().get('/login/signup/')
        self.assertTemplateUsed(response, 'signup.html')

    def test_logInAndGreet_signup_function_correct(self):
        found = resolve('/login/signup/')
        self.assertEqual(found.func, signUpAcc)

class LogInAndGreetFunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.driver = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        self.driver.get(self.live_server_url + '/login/')
        super(LogInAndGreetFunctionalTest, self).setUp()

    def tearDown(self):
        self.driver.quit()
        super(LogInAndGreetFunctionalTest, self).tearDown()

    def test_sign_up_log_out_wrong_password_and_log_in(self):
        signUpButton = self.driver.find_element_by_id('signupButton')
        signUpButton.click()

        self.driver.implicitly_wait(5)

        usernameBox = self.driver.find_element_by_id('id_username')
        passwordBox = self.driver.find_element_by_id('id_password1')
        conf_passwordBox = self.driver.find_element_by_id('id_password2')
        submit_button = self.driver.find_element_by_id('signupCreateButton')

        usernameBox.send_keys("dummyTest")
        passwordBox.send_keys("ghaksc24029")
        conf_passwordBox.send_keys("ghaksc24029")

        submit_button.click()

        self.driver.implicitly_wait(5)

        logoutButton = self.driver.find_element_by_id('logoutButton')
        logoutButton.click()

        self.driver.implicitly_wait(5)

        usernameBox = self.driver.find_element_by_id('id_username')
        passwordBox = self.driver.find_element_by_id('id_password')

        login_submit_button = self.driver.find_element_by_id('loginButton')

        usernameBox.send_keys("dummyTest")
        passwordBox.send_keys("testWrongPassword")

        login_submit_button.click()

        self.driver.implicitly_wait(5)

        usernameBox = self.driver.find_element_by_id('id_username')
        passwordBox = self.driver.find_element_by_id('id_password')

        usernameBox.clear()
        passwordBox.clear()

        login_submit_button = self.driver.find_element_by_id('loginButton')

        usernameBox.send_keys("dummyTest")
        passwordBox.send_keys("ghaksc24029")

        login_submit_button.click()

        self.driver.implicitly_wait(5)

        username_display = self.driver.find_element_by_id('username').text

        self.assertEqual(username_display, "dummyTest")
