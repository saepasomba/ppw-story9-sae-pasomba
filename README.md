[![pipeline status](https://gitlab.com/saepasomba/ppw-story9-sae-pasomba/badges/master/pipeline.svg)](https://gitlab.com/saepasomba/ppw-story9-sae-pasomba/commits/master)
[![coverage report](https://gitlab.com/saepasomba/ppw-story9-sae-pasomba/badges/master/coverage.svg)](https://gitlab.com/saepasomba/ppw-story9-sae-pasomba/commits/master)


> Losing is part of life. This **isn't tough**, this is **life**.