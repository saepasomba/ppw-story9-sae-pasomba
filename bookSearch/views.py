from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.core import serializers

from .models import LikedBooks

# Create your views here.
def showFinder(request):
    return render(request, 'index.html')

def getLikes(request, bookID):
    try:
        bookTarget = LikedBooks.objects.get(bookID = bookID)
        return JsonResponse({"likes" : bookTarget.likes})
    except:
        return JsonResponse({"likes" : 0})

def addLike (request):
    if request.method == "POST":
        bookID = request.POST["bookID"]
        
        try:
            bookTarget = LikedBooks.objects.get(bookID = bookID)
            bookTarget.likes += 1
            bookTarget.save()
        except:
            LikedBooks.objects.create (
                bookID = request.POST["bookID"],
                title = request.POST["title"],
                thumbnail = request.POST["thumbnail"],
                description = request.POST["description"]
            )
    
    return redirect ("bookSearch:showFinder")

def mostLiked(request):
    return JsonResponse({"topBooks" : list(LikedBooks.objects.order_by("-likes")[:5].values())})

