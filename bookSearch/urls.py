from django.urls import path
from . import views

app_name = 'bookSearch'

urlpatterns = [
    path('', views.showFinder, name='showFinder'),
    path('getLikes/<str:bookID>', views.getLikes, name='getLikes'),
    path('addLike/', views.addLike, name='addLike'),
    path('mostLiked/', views.mostLiked, name='mostLiked')
]
