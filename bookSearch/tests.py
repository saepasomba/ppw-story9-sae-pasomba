from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

from .models import *
from .views import *
from .apps import *

import time
import os

class UnitTestsBookSearch(TestCase):

    def test_apps(self):
        self.assertEqual(BooksearchConfig.name, 'bookSearch')
        self.assertEqual(apps.get_app_config('bookSearch').name, 'bookSearch')

    def test_index_routing_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_index_routing_correct(self):
        function = resolve('/')
        self.assertEqual(function.func, showFinder)

    def test_index_html_render(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_object_naming_modal(self):
        dummyObject = LikedBooks.objects.create(
            bookID = "id1123123123",
            title = "dummyTitle",
            thumbnail = "dummy.com",
            description = "loremipsumblabla",
            likes = 1
        )

        self.assertEqual(dummyObject.__str__(), "dummyTitle")


class FunctionalTestBookSearch(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTestBookSearch, self).setUp()

    def tearDown(self):
        self.driver.quit()
        super(FunctionalTestBookSearch, self).tearDown()

    def test_search_functionality(self):
        self.driver.get(self.live_server_url)
        searchBox = self.driver.find_element_by_id("searchBox")
        searchButton = self.driver.find_element_by_id('searchButton')

        searchBox.send_keys("test")
        time.sleep(3)
        searchButton.click()
        time.sleep(3)
        self.assertIn("test", self.driver.page_source)
        

    def test_like_functionality(self):
        self.driver.get(self.live_server_url)

        searchBox = self.driver.find_element_by_id("searchBox")
        searchButton = self.driver.find_element_by_id('searchButton')

        searchBox.send_keys("test")
        time.sleep(3)
        searchButton.click()
        time.sleep(3)
        likesBefore = self.driver.find_element_by_class_name("likeCounter").text

        likeButton = self.driver.find_element_by_class_name("likeButton")

        likeButton.click()

        time.sleep(2)

        likeButton.click()

        likesAfter = self.driver.find_element_by_class_name("likeCounter").text

        self.assertNotEqual(likesBefore, likesAfter)

    def test_modal_show(self):
        self.driver.get(self.live_server_url)

        modalButton = self.driver.find_element_by_class_name("topButton")
        time.sleep(3)
        modalButton.click() 
        time.sleep(3)

        self.assertIn("Most liked books below!", self.driver.page_source)       
