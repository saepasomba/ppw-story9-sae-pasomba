from django.db import models

# Create your models here.
class LikedBooks(models.Model):
    bookID = models.CharField(max_length=150)
    title = models.CharField(max_length=150)
    thumbnail = models.URLField()
    description = models.TextField()
    likes = models.IntegerField(default=1)

    def __str__(self):
        return self.title